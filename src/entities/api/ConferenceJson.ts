import { DateJson } from "./DateJson";
import { LocationJson } from "./LocationJson";

export interface ConferenceJson {
  name: string;
  url: string;
  location: LocationJson;
  date: DateJson;
  tags: string[];
  cfp: {
    has_cfp: boolean;
    link: string;
  };
  coc: {
    has_coc: boolean;
    link: string;
  };
}
